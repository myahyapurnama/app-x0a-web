<?php
$DB_NAME = "kampus";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    $data_mhs = array();
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT m.nim,m.nama,p.nama_prodi,concat('http://192.168.1.8/kampus/images/',photos) as url, m.alamat,j.kelamin
        FROM mahasiswa m,prodi p , jenis_kelamin j
        WHERE m.id_prodi=p.id_prodi AND m.id_jk = j.id_jk";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            while($mhs = mysqli_fetch_assoc($result)){
                array_push($data_mhs,$mhs);
            }
        }
    }
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>KAMPUS</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="mahasiswa.php">Data Mahasiswa</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="prodi.php">Data Prodi</a>
                </li>
            </ul>
        </div>
    </nav>
    <br>
    <div class="container">
        <h1> Data Mahasiswa </h1>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead class="table-striped" style="text-align: center;">
                    <tr class="bg-dark text-light">
                        <th scope="col">No</th>
                        <th scope="col">NIM</th>
                        <th scope="col">Nama Mahasiswa</th>
                        <th scope="col">Jenis Kelamin</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Prodi</th>
                        <th scope="col">Photo</th>
                    </tr>
                </thead>
                <?php
                    $no=1;
                    foreach ($data_mhs as $mhs) {
                ?>
                <tbody>
                    <tr class="bg-light">
                        <th scope="row" style="text-align: center;"><?php echo $no;$no++; ?></th>
                        <td class="text-center"><?php echo $mhs['nim']; ?></td>
                        <td><?php echo $mhs['nama']; ?></td>
                        <td class="text-center"><?php echo $mhs['kelamin']; ?></td>
                        <td><?php echo $mhs['alamat']; ?></td>
                        <td><?php echo $mhs['nama_prodi']; ?></td>
                        <td style="text-align: center;">
                            <img src="<?php echo $mhs['url']; ?>" style="width: 200px;" alt="Foto Mahasiswa">
                        </td>
                    </tr>
                    </tr>
                </tbody>
                <?php } ?>
            </table>
        </div>
    </div>

</body>

</html>